# FLUXvisualizer

Welcome to the FLUXvisualizer. This software is able to visualize flux trough a (metabolic) network.

It uses python3 und is published under the GNU public license version 3. 

A manual is available [here](https://fluxvisualizer.ibgc.cnrs.fr/manual.pdf).

The program was developed in the University of Bordeaux, France at the Institute de Biochimie et Génétique Cellulaires under supervision of Jean-Pierre Mazat and supported by the Plan cancer 2014-2019 No BIO 2014 06 and the French Association against Myopathies.

<br>
To start the program type in a command window:

$ python3 YOURPATH/FLUXvisualizerGUI.py
<br>
<br>
