#!/usr/bin/python3

from math import log10
from lxml import etree as ET
import re

__author__ = 'Tim Daniel Rose'
__credits__ = 'Jean-Pierre Mazat'
__email__ = 'tim@rose-4-you.de'


class FLUXvisualizer:
    def __init__(self):
        self.path = []
        self.file = str()
        self.svg = 0
        self.svg_superlist = []
        self.reacFormat = 'REACTION_COUNTER'
        self.widthFactor = 2
        self.widthFacBackup = self.widthFactor
        self.stoichiometricFac = []
        self.widthl = []
        self.dynW = 0
        self.metatoolfile = ''
        self.path_superlist = []
        self.multi = False
        self.orgpath = ''
        self.multifileext = '_EFM_'
        self.roundn = 7

        # Parameters for writing down The Path
        self._newtextID = 'FLUX'
        self._PathTXTlineheight = 1
        self._ReacPerLine = 3
        self._TitleReacDiff = 0.6
        self._PathName = 'FLUX'

        # Color of the Written Flux Value
        self.chColor = '0'
        self.chFColor = '0'
        self.colorl = {'0': False, '1': '#a01700',  # red
                       '2': '#00ff00',  # green
                       '3': '#ffffff',  # white
                       '4': '#000000',  # black
                       '5': '#0000ff',  # blue
                       '6': '#e87e00'}  # orange
        self.FluxSizeFac = 2

    def input_file(self, file):
        self.file = file
        xml = ET.parse(self.file)
        self.svg = xml.getroot()
        self.svg_superlist = []

    def input_path(self, path):
        self.path = []
        self.stoichiometricFac = []
        self.orgpath = path
        a = True
        b = True
        bracket = 0
        element = 0
        # Delete spacing at end end beginning:
        while a:
            if path[0] == ' ':
                path = path[1:]
            else:
                a = False
        while b:
            if path[-1] == ' ':
                path = path[:-1]
            else:
                b = False
        path = path.strip()
        first = path.split(' ')
        i = 0
        while i < len(first):
            if first[i][0] == '-':
                self.stoichiometricFac.append(-1.)
                self.path.append(first[i][1:])
                i += 1
            elif first[i][0] == '(':
                self.stoichiometricFac.append(float(first[i][1:]))
                self.path.append(first[i+1][:-1])
                i += 2
            else:
                self.stoichiometricFac.append(1.)
                self.path.append(first[i])
                i += 1

    def input_CNApath(self, path):
        self.path = []
        self.stoichiometricFac = []
        self.orgpath = path
        a = True
        b = True
        bracket = 0
        element = 0
        # Delete spacing at end end beginning:
        while a:
            if path[0] == ' ':
                path = path[1:]
            else:
                a = False
        while b:
            if path[-1] == ' ':
                path = path[:-1]
            else:
                b = False
        path = path.strip()
        first = path.split(' ')
        i = 0
        while i < len(first):
            if first[i] == '':
                del first[i]
            else:
                i += 1
        i = 0
        while i < len(first):
            self.stoichiometricFac.append(float(first[i]))
            self.path.append(first[i+1])
            i += 2

    def make_widthl(self, f1, f2, f3, w1, w2, w3, w4):
        self.widthl = []
        for d in range(len(self.path)):
            if abs(self.stoichiometricFac[d]) < f1:
                self.widthl.append(w1)
            elif f1 <= abs(self.stoichiometricFac[d]) < f2:
                self.widthl.append(w2)

            elif f2 <= abs(self.stoichiometricFac[d]) < f3:
                self.widthl.append(w3)

            elif f3 <= abs(self.stoichiometricFac[d]):
                self.widthl.append(w4)

    def make_autowidth(self, wmin, wmax, logarithmic=False):
        self.widthl = []
        absw = [abs(d) for d in self.stoichiometricFac]
        if not logarithmic:
            absw2 = [e for e in absw if e != 0]
        else:
            absw2 = []
            for e in absw:
                if e != 0 and e != 1:
                    absw2.append(log10(e))
                elif e == 1:
                    absw2.append(log10(e+0.001))
        if max(absw2) != min(absw2):
            slope = (wmax-wmin)/(max(absw2)-min(absw2))
            n = wmin - slope*min(absw2)
            if logarithmic:
                self.widthl = [slope*log10(x) + n if x != 0 else 1 for x in absw]
            else:
                self.widthl = [slope * x + n if x != 0 else 1 for x in absw]
        else:
            self.widthl = [wmax for x in absw]

    def getall_children(self):
        counter = 0

        for i in range(len(self.svg.getchildren())):
            self.svg_superlist.append(self.svg.getchildren()[i])

        while counter < len(self.svg_superlist):
            tmpchildren = self.svg_superlist[counter].getchildren()
            for j in tmpchildren:
                self.svg_superlist.append(j)
            counter += 1

    def reactionfinder(self, name):
        for i in self.svg_superlist:
            for j in i.values():
                if re.match(name, j):
                    print(j)
                    self.element = i
                    self.changewidth()
                # counter = 0
                # for n in range(len(name)):
                #     if name[n] == j[n]:
                #         counter += 1
                # if len(name) == counter:

    def changewidth(self):
        try:
            m = re.search('(?<=width:)([0-9]+\.*[0-9]*)', self.element.attrib['style'])
            tmp_width = float(m.group(0))
            tmp_width *= self.widthFactor
            self.element.attrib['style'] = self.element.attrib['style'].replace('width:'+m.group(0),
                                                                                'width:'+str(tmp_width))
            if self.colorl[self.chFColor]:
                m = re.search('(?<=stroke:)(.*?);', self.element.attrib['style'])
                tmp_color = m.group(0)
                self.element.attrib['style'] = self.element.attrib['style'].replace('stroke:' + tmp_color, 'stroke:' +
                                                                                    self.colorl[self.chFColor] + ';')

        except AttributeError:
            pass

    def visualize_path(self):
        for i in range(len(self.path)):
            tmp_reac = self.reacFormat.replace('REACTION', self.path[i]).replace('COUNTER', '[1-9]+')
            tmp_reac += '$'
            tmp_reac = '^' + tmp_reac
            if 'REV' in tmp_reac:
                if self.stoichiometricFac[i] < 0:
                    tmp_reac = tmp_reac.replace('REV', '-')
                elif self.stoichiometricFac[i] >= 0:
                    tmp_reac = tmp_reac.replace('REV', '')

            if self.dynW:
                self.dynamic_width(i)

            if self.stoichiometricFac[i] != 0:
                self.reactionfinder(tmp_reac)

    def change_dynW(self, yn):
        if self.dynW == 1:
            # dynW
            if yn == 1:
                pass
            elif yn == 0:
                self.widthFactor = self.widthFacBackup
                self.dynW = 1
            elif yn == 2:
                self.dynW = 2

        elif self.dynW == 0:
            # one width
            if yn == 1:
                self.widthFacBackup = self.widthFactor
                self.dynW = 1
            elif yn == 2:
                self.widthFacBackup = self.widthFactor
                self.dynW = 2
            elif not yn:
                pass

        elif self.dynW == 2:
            # adapt width
            if yn == 1:
                self.dynW = 1
            elif yn == 0:
                self.widthFactor = self.widthFacBackup
                self.dynW = 0
            elif yn == 2:
                pass

    def dynamic_width(self, n):
        self.widthFactor = self.widthl[n]

    def write_path(self, n):
        found = False
        for i in self.svg_superlist:
            if 'tspan' in i.tag:
                try:
                    if 'place_here' in i.text:
                        i.text = n
                        found = True
                        break
                except TypeError:
                    pass

        if found:
            # make new textelement with different fontsize
            ip1 = i.getparent()
            ip2 = ip1.getparent()
            newtext = ET.Element(ip1.tag)
            for j in ip1.attrib:
                newtext.attrib[j] = ip1.attrib[j]

            newtext.attrib['id'] = self._newtextID
            newtspan = ET.Element(i.tag)
            for k in i.attrib:
                newtspan.attrib[k] = i.attrib[k]
            newtspan.attrib['id'] = self._newtextID+'path'
            # change size of letters
            m = re.search('(?<=font-size:)(.*?)px;', newtext.attrib['style'])
            tmp_size = float(m.group(0)[:-3])
            tmp_size *= self._TitleReacDiff
            newtext.attrib['style'] = newtext.attrib['style'].replace('font-size:' + m.group(0),
                                                                      'font-size:' + str(tmp_size) + ';')
            newtspan.attrib['y'] = str(float(newtspan.attrib['y']) + self._PathTXTlineheight*(tmp_size/self._TitleReacDiff))
            newtext.attrib['y'] = newtspan.attrib['y']
            lines = len(self.path)//self._ReacPerLine + 1
            tspanl = []
            for l in range(lines):
                tspanl.append(ET.Element(i.tag))
                for atts in i.attrib:
                    tspanl[-1].attrib[atts] = newtspan.attrib[atts]
                tspanl[-1].text = ''
                tspanl[-1].attrib['y'] = str(float(newtspan.attrib['y']) + (self._PathTXTlineheight*(tmp_size / self._TitleReacDiff)*l))

            for m in range(len(self.path)):
                tspanl[m//self._ReacPerLine].text += str(self.stoichiometricFac[m]) + ' ' + self.path[m] + '    '
            for n in tspanl:
                newtext.append(n)
            ip2.append(newtext)


        else:
            print('EFMvisualizer did not find a place to write down the EFM')

    def save(self, output):
        tree = ET.ElementTree(self.svg)
        tree.write(output, pretty_print=True, xml_declaration=True,   encoding="utf-8")

    def parse_metatool(self, metaf):
        f = open(metaf, 'r')
        EM = False
        start = False
        pstart = False
        for line in f:
            if EM and start:
                tmppath = ''
                for i in line:
                    if pstart:
                        tmppath += i
                    if i == ')':
                        pstart = True
                self.path_superlist.append(tmppath.strip())
                pstart = False
                if self.path_superlist[-1] == '':
                    del self.path_superlist[-1]
                    break
            if '# in () indicates # of enzymes used by the elementary mode' in line:
                start = True
            if 'ELEMENTARY MODES' in line:
                EM = True
        f.close()

    def parse_CNAlist(self, cnal):
        f = open(cnal, 'r')
        for line in f:
            self.path_superlist.append(line)
        f.close()

    def visualize_cnal(self, outscheme, write=False, writef=False, widthpar=(1, 2, 3, 2, 3, 4, 5), autow=(1.5, 3), logarithmic=False):
        for i in range(len(self.path_superlist)):
            self.input_CNApath(self.path_superlist[i])
            self.input_file(self.file)
            self.getall_children()
            if self.dynW == 1:
                self.make_widthl(widthpar[0], widthpar[1], widthpar[2], widthpar[3], widthpar[4],
                                 widthpar[5], widthpar[6])
            elif self.dynW == 2:
                self.make_autowidth(autow[0], autow[1], logarithmic=logarithmic)
            self.visualize_path()
            self.round_stoich()
            if write:
                self.write_path(self._PathName+str(i+1))
            if writef:
                self.write_flux()
            tmp = outscheme.split('.')
            self.save(tmp[0]+self.multifileext+str(i+1)+'.svg')
        self.EFMl = len(self.path_superlist)
        self.path_superlist = []
        print(self.EFMl)

    def visualize_all(self, outscheme, write=False, writef=False, widthpar=(1, 2, 3, 2, 3, 4, 5), autow=(1.5, 3), logarithmic=False):
        for i in range(len(self.path_superlist)):
            self.input_path(self.path_superlist[i])
            self.input_file(self.file)
            self.getall_children()
            if self.dynW == 1:
                self.make_widthl(widthpar[0], widthpar[1], widthpar[2], widthpar[3], widthpar[4],
                                 widthpar[5], widthpar[6])
            elif self.dynW == 2:
                self.make_autowidth(autow[0], autow[1], logarithmic=False)

            self.visualize_path()
            self.round_stoich()
            if write:
                self.write_path(self._PathName+str(i+1))
            if writef:
                self.write_flux()
            tmp = outscheme.split('.')
            self.save(tmp[0]+self.multifileext+str(i+1)+'.svg')
        self.EFMl = len(self.path_superlist)
        self.path_superlist = []

    def write_flux(self):
        new_structure = False
        for z in range(len(self.path)):
            found = False
            for i in self.svg_superlist:
                if 'tspan' in i.tag:
                    try:
                        if re.match(self.path[z]+'$', i.text):
                            found = True
                            break
                    except TypeError:
                        pass
            if found:
                ip10 = i.getparent()
                newnewtspan = ""
                if 'tspan' in ip10.tag:
                    ip1 = ip10.getparent()
                    new_structure = True
                else:
                    ip1 = i.getparent()
                    new_structure=False
                ip2 = ip1.getparent()
                newtext = ET.Element("text")
                for j in ip1.attrib:
                    newtext.attrib[j] = ip1.attrib[j]

                newtext.attrib['id'] = 'TEXT_'+self.path[z]
                if new_structure:
                    newnewtspan = ET.Element("tspan")
                    for k in ip10.attrib:
                        newnewtspan.attrib[k] = ip10.attrib[k]
                    newnewtspan.attrib['id'] = 'TSPANTSPAN_'+self.path[z]
                newtspan = ET.Element("tspan")
                for k in i.attrib:
                    newtspan.attrib[k] = i.attrib[k]
                newtspan.attrib['id'] = 'TSPAN_'+self.path[z]
                
                try:
                    m = re.search('(?<=font-size:)([0-9]+\.*[0-9]*)', newtext.attrib['style'])
                    tmp_size = float(m.group(0))
                    newtext.attrib['style'] = newtext.attrib['style'].replace('font-size:'+m.group(0),
                                                                              'font-size:' +
                                                                              str(tmp_size*self.FluxSizeFac))
                    newtext.attrib['style'] = re.sub(r'shape-inside\:url\(#[a-zA-Z0-9]+\);',r'', newtext.attrib['style'])

                except AttributeError:
                    scheme = '(?<=font-size:)([1-9]+\.*[1-9]*)'
                    m = re.search('(?<=font-size:)([0-9]+\.*[0-9]*)', newtspan.attrib['style'])
                    tmp_size = float(m.group(0))
                    newtspan.attrib['style'] = newtspan.attrib['style'].replace('font-size:' + m.group(0),
                                                                                'font-size:' +
                                                                                str(tmp_size * self.FluxSizeFac))
                    newtspan.attrib['style'] = re.sub(r'shape-inside\:url\(#[a-zA-Z0-9]+\);',r'', newtspan.attrib['style'])
                if new_structure:
                    newnewtspan.attrib['y'] = str(
                        float(newnewtspan.attrib['y']) + self._PathTXTlineheight * tmp_size * self.FluxSizeFac)
                    newtspan.text = 'X '+str(self.stoichiometricFac[z])
                else:
                    newtspan.attrib['y'] = str(
                        float(newtspan.attrib['y']) + self._PathTXTlineheight * tmp_size * self.FluxSizeFac)
                    newtext.attrib['y'] = newtspan.attrib['y']
                    newtspan.text = 'X '+str(self.stoichiometricFac[z])

                if self.colorl[self.chColor]:
                    try:
                        m = re.search('(?<=fill:)(.*?);', newtext.attrib['style'])
                        try:
                            tmp_color = m.group(0)
                            newtext.attrib['style'] = newtext.attrib['style'].replace('fill:'+tmp_color, 'fill:' +
                                                                                      self.colorl[self.chColor]+';')
                        except AttributeError:
                            newtext.attrib['style'] = newtext.attrib['style'] + 'fill:' + self.colorl[self.chColor]+ ';'

                    except AttributeError:
                        m = re.search('(?<=fill:)(.*?);', newtspan.attrib['style'])
                        tmp_color = m.group(0)
                        newtspan.attrib['style'] = newtspan.attrib['style'].replace('fill:' + tmp_color, 'fill:' +
                                                                                  self.colorl[self.chColor] + ';')
                if new_structure:
                    newnewtspan.append(newtspan)
                    newtext.append(newnewtspan)
                    ip2.append(newtext)
                else:
                    newtext.append(newtspan)
                    ip2.append(newtext)

    def input_copasi(self, file):
        f = open(file, 'r')
        self.path = []
        self.stoichiometricFac = []
        found = False
        for lines in f:

            if found:
                tmp = lines.strip().split('\t')
                if len(tmp) > 1:
                    self.path.append(tmp[0])
                    self.stoichiometricFac.append(float(tmp[1]))
            if 'Reaction\tFlux' in lines:
                found = True

        f.close()

    def round_stoich(self):
        self.stoichiometricFac = [round(x, self.roundn) for x in self.stoichiometricFac]


def main():
    vis = FLUXvisualizer()
    vis.input_file('ExampleNetwork.svg')
    vis.getall_children()
    vis.input_path('T6 (8 T9)')
    vis.change_dynW(2)
    vis.make_autowidth(1.2, 6.0, logarithmic=False)
    print(vis.stoichiometricFac)
    print(vis.widthl)
    vis.visualize_path()
    #vis.write_path('FLUX')
    #vis.write_flux()
    vis.save('test3.svg')

if __name__ == '__main__':
    main()
