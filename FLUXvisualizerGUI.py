#!/usr/bin/python3

from FLUXvisualizerAPI import *
from tkinter import *
from tkinter import filedialog, messagebox, font


class GUI:
    def __init__(self):
        self.master = Tk()
        self.master.title('FluxVisualizer')
        self.outfile = ''
        self.vis = FLUXvisualizer()
        self.met = StringVar()
        self.metaf = ''
        self.write = IntVar()
        self.writef = IntVar()
        self.widthflux = [1, 2, 3, 2, 3, 4, 5]
        self.roundn = StringVar()
        self.roundn.set('7')
        self.logarithmic = IntVar()

        # Fonts:
        self.font2 = font.Font(weight='bold', size=10)
        self.butfont = font.Font(weight='bold')
        # Window Size
        w = 575
        h = 295
        x = (self.master.winfo_screenwidth() - w)/2
        y = (self.master.winfo_screenheight() - h)/2
        self.master.geometry('%dx%d+%d+%d' % (w, h, x, y))

        # Width Factor
        # # simple width
        Label(self.master, text='Choose a width format:', font=self.font2).grid(row=2, column=0, columnspan=5, sticky=W)
        Label(self.master, text='Width factor:').grid(row=4, column=0, sticky=E)
        self.e2 = Entry(self.master, bg='white', width=4)
        self.e2.insert(0, str(self.vis.widthFactor))
        self.e2.grid(row=4, column=1, sticky=W)
        self.dw = IntVar()
        self.dw.set(0)

        # # dynamic width
        Label(self.master, text='Flux boundaries:').grid(row=9, column=0, sticky=E)
        Label(self.master, text='Width factor:').grid(row=10, column=0, sticky=E)
        Label(self.master, text='<').grid(column=1, row=9)
        self.f1 = Entry(self.master, bg='white', width=4)
        self.f1.grid(column=2, row=9, sticky=W)
        Label(self.master, text='<=').grid(column=3, row=8)
        self.f2 = Entry(self.master, bg='white', width=4)
        self.f2.grid(column=4, row=9, sticky=W)
        Label(self.master, text='<=').grid(column=5, row=8)
        self.f3 = Entry(self.master, bg='white', width=4)
        self.f3.grid(column=6, row=9, sticky=W)
        Label(self.master, text='<').grid(column=7, row=8)

        self.w1 = Entry(self.master, bg='white', width=4)
        self.w1.grid(column=1, row=10, sticky=W)
        self.w2 = Entry(self.master, bg='white', width=4)
        self.w2.grid(column=3, row=10, sticky=W)
        self.w3 = Entry(self.master, bg='white', width=4)
        self.w3.grid(column=5, row=10, sticky=W)
        self.w4 = Entry(self.master, bg='white', width=4)
        self.w4.grid(column=7, row=10, sticky=W)

        # adapt width
        Label(self.master, text='min. width:').grid(row=6, column=0, columnspan=2, sticky=E)
        Label(self.master, text='max. width:').grid(row=6, column=4, columnspan=2, sticky=E)
        self.wmin = Entry(self.master, bg='white', width=4)
        self.wmin.insert(0, str(1.2))
        self.wmin.grid(column=2, row=6, sticky=W)
        self.wmax = Entry(self.master, bg='white', width=4)
        self.wmax.insert(0, str(2.5))
        self.wmax.grid(column=6, row=6, sticky=W)
        Checkbutton(self.master, variable=self.logarithmic, text='Logarithmic').grid(column=0, row=7, columnspan=2, sticky=E)


        # choose which width type
        rb1 = Radiobutton(self.master, variable=self.dw, value=0, text='Constant width',
                          command=self.widthCheck)
        rb1.grid(row=3, column=0, columnspan=2, sticky=W)
        rb2 = Radiobutton(self.master, variable=self.dw, value=1, text='Variable width',
                          command=self.widthCheck)
        rb2.grid(row=8, column=0, columnspan=2, sticky=W)
        rb3 = Radiobutton(self.master, variable=self.dw, value=2, text='Auto width',
                          command=self.widthCheck)
        rb3.grid(row=5, column=0, columnspan=2, sticky=W)

        self.widthCheck()

        # ID format
        Label(self.master, text='ID format:').grid(row=0, column=0, rowspan=2, sticky=E)
        self.e4 = Entry(self.master, bg='white')
        self.e4.grid(row=0, column=1, rowspan=2, columnspan=7, sticky=W)
        self.e4.insert(0, str(self.vis.reacFormat))

        # Choose input
        Label(self.master, text='Choose a flux input format:', font=self.font2).grid(row=0, column=8)
        self.inp = IntVar()
        self.inp.set(0)
        Radiobutton(self.master, text='single flux (metatool)', padx=20, variable=self.inp, value=0).grid(row=1,
                                                                                                          column=8,
                                                                                                          sticky=W)
        Radiobutton(self.master, text='metatool Output file', padx=20, variable=self.inp, value=1).grid(row=2,
                                                                                                        column=8,
                                                                                                        sticky=W)
        Radiobutton(self.master, text='single flux (CNA export) ', padx=20, variable=self.inp,
                    value=2).grid(row=3, column=8, sticky=W)
        Radiobutton(self.master, text='CNA export list', padx=20, variable=self.inp, value=3).grid(row=4, column=8,
                                                                                                   sticky=W)
        Radiobutton(self.master, text='COPASI export', padx=20, variable=self.inp, value=4).grid(row=5, column=8,
                                                                                                 sticky=W)
        Radiobutton(self.master, text='FAME export', padx=20, variable=self.inp, value=5).grid(row=6, column=8,
                                                                                                 sticky=W)

        # Open file
        Label(self.master, text='  ').grid(row=10, column=0)
        browsebutton = Button(self.master, text="Open svg-file", command=self.browsefunc)
        browsebutton.grid(column=0, row=11, columnspan=8)
        self.opened = StringVar()
        self.showfile = StringVar()
        self.opened.set('Currently no loaded file')
        self.filename = ''
        Label(self.master, textvariable=self.opened).grid(row=12, column=0, columnspan=8)
        Label(self.master, textvariable=self.showfile).grid(row=13, column=0, columnspan=8, rowspan=1)


        # Saving variables
        self.saved = StringVar()
        self.saved.set('Define a file to save output')
        self.showout = StringVar()

        # Continue Button

        Button(self.master, text='Continue', command=self.visualize, height=2, width=10, font=self.butfont).grid(row=7,
                                                                                                            column=8,
                                                                                                            rowspan=2)

        # Menu
        menu = Menu(self.master)
        self.master.config(menu=menu)
        ## Settings
        advanced = Menu(menu)
        menu.add_cascade(label='Settings', menu=advanced)
        advanced.add_command(label='Path text Settings', command=self.advpar)
        advanced.add_command(label='Flux value Settings', command=self.fluxpar)
        advanced.add_command(label='Settings for multiple visualisations', command=self.multipar)
        advanced.add_command(label='Reaction arrow Settings', command=self.fluxpar2)

        ## Help
        helpm = Menu(menu)
        menu.add_cascade(label='Help', menu=helpm)
        helpm.add_command(label='Author information', command=self.authorinfo)
        helpm.add_command(label='Manual', command=self.manual)

    def manual(self):
        self.manu = Toplevel()
        self.manu.title('Manual')
        w = 270
        h = 180
        x = (self.manu.winfo_screenwidth() - w) / 2
        y = (self.manu.winfo_screenheight() - h) / 2
        self.manu.geometry('%dx%d+%d+%d' % (w, h, x, y))

        Label(self.manu, text='A manual of the software is available at:').grid(row=0, column=0)
        ent = Entry(self.manu)
        ent.grid(row=1, column=0)
        ent.insert(0, 'https://fluxvisualizer.ibgc.cnrs.fr/manual.pdf')
        ent.config(state='readonly')
        Label(self.manu, text='  ').grid(row=2, column=0)
        Label(self.manu, text='For further information, suggestions or').grid(row=3, column=0)
        Label(self.manu, text='bugfixes, contact the author.').grid(row=4, column=0)

    def authorinfo(self):
        self.ai = Toplevel()
        self.ai.title('Author information')
        w = 280
        h = 180
        x = (self.ai.winfo_screenwidth() - w) / 2
        y = (self.ai.winfo_screenheight() - h) / 2
        self.ai.geometry('%dx%d+%d+%d' % (w, h, x, y))

        # Author information
        self.infofont = font.Font(size=6)
        Label(self.ai, text='Author: Tim Daniel Rose').grid(row=0, column=0)
        Label(self.ai, text='E-Mail: tim@rose-4-you.de').grid(row=1, column=0)
        Label(self.ai, text='  ').grid(row=2, column=0)
        Label(self.ai, text='License: GPLv3').grid(row=3, column=0)
        Label(self.ai, text='  ').grid(row=4, column=0)
        Label(self.ai, text='The software was developed at the:').grid(row=5, column=0)
        Label(self.ai, text='Université de Bordeaux (France)').grid(row=6, column=0)
        Label(self.ai, text='Institut de Biochimie et Génétique Cellulaires').grid(row=7, column=0)
        Label(self.ai, text='under supervision of Jean-Pierre Mazat').grid(row=8, column=0)

    def widthCheck(self):
        if self.dw.get() == 0:
            self.e2.config(state='normal')
            self.f1.config(state='disabled')
            self.f2.config(state='disabled')
            self.f3.config(state='disabled')
            self.w1.config(state='disabled')
            self.w2.config(state='disabled')
            self.w3.config(state='disabled')
            self.w4.config(state='disabled')
            self.wmin.config(state='disabled')
            self.wmax.config(state='disabled')
            self.vis.change_dynW(0)
        elif self.dw.get() == 1:
            self.vis.change_dynW(1)
            self.e2.config(state='disabled')
            self.f1.config(state='normal')
            self.f2.config(state='normal')
            self.f3.config(state='normal')
            self.w1.config(state='normal')
            self.w2.config(state='normal')
            self.w3.config(state='normal')
            self.w4.config(state='normal')
            self.wmin.config(state='disabled')
            self.wmax.config(state='disabled')
        elif self.dw.get() == 2:
            self.e2.config(state='disabled')
            self.f1.config(state='disabled')
            self.f2.config(state='disabled')
            self.f3.config(state='disabled')
            self.w1.config(state='disabled')
            self.w2.config(state='disabled')
            self.w3.config(state='disabled')
            self.w4.config(state='disabled')
            self.wmin.config(state='normal')
            self.wmax.config(state='normal')
            self.vis.change_dynW(2)

    def fluxpar(self):
        self.adv2 = Toplevel()
        self.adv2.title('Flux Value Settings')
        w = 200
        h = 285
        x = (self.adv2.winfo_screenwidth() - w) / 2
        y = (self.adv2.winfo_screenheight() - h) / 2
        self.adv2.geometry('%dx%d+%d+%d' % (w, h, x, y))
        self.col = StringVar()
        self.col.set(self.vis.chColor)
        Label(self.adv2, text='Color of the written flux value:').grid(column=0, row=0, columnspan=2, sticky=W)
        Radiobutton(self.adv2, text='Use default', padx=20, variable=self.col, value='0').grid(column=0,
                                                                                               row=1,
                                                                                               sticky=W)
        Radiobutton(self.adv2, text='Red', padx=20, variable=self.col, value='1').grid(column=0, row=2, sticky=W)
        Radiobutton(self.adv2, text='Green', padx=20, variable=self.col, value='2').grid(column=0, row=3, sticky=W)
        Radiobutton(self.adv2, text='White', padx=20, variable=self.col, value='3').grid(column=0, row=4, sticky=W)
        Radiobutton(self.adv2, text='Black', padx=20, variable=self.col, value='4').grid(column=0, row=5, sticky=W)
        Radiobutton(self.adv2, text='Blue', padx=20, variable=self.col, value='5').grid(column=0, row=6, sticky=W)
        Radiobutton(self.adv2, text='Orange', padx=20, variable=self.col, value='6').grid(column=0, row=7, sticky=W)

        Label(self.adv2, text='Size factor of flux value:').grid(row=8, column=0, sticky=W)
        self.fsf = StringVar()
        self.fsf.set(str(self.vis.FluxSizeFac))
        Entry(self.adv2, textvariable=self.fsf, bg='white', width=5).grid(row=8, column=1)

        Label(self.adv2, text='Decimals of flux value:').grid(column=0, row=9)
        sf = Entry(self.adv2, bg='white', textvariable=self.roundn, width=5)
        sf.grid(column=1, row=9)
        Button(self.adv2, text='Apply', command=self.applyADV2).grid(row=10, column=0, columnspan=2)

    def fluxpar2(self):
        self.adv3 = Toplevel()
        self.adv3.title('Reaction arrow Settings')
        w = 230
        h = 200
        x = (self.adv3.winfo_screenwidth() - w) / 2
        y = (self.adv3.winfo_screenheight() - h) / 2
        self.adv3.geometry('%dx%d+%d+%d' % (w, h, x, y))
        self.col2 = StringVar()
        self.col2.set(self.vis.chFColor)
        Label(self.adv3, text='Color of the updated reaction arrow:').grid(column=0, row=0, columnspan=2, sticky=W)
        Radiobutton(self.adv3, text='Use default', padx=20, variable=self.col2, value='0').grid(column=0,
                                                                                               row=1,
                                                                                               sticky=W)
        Radiobutton(self.adv3, text='Red', padx=20, variable=self.col2, value='1').grid(column=0, row=2, sticky=W)
        Radiobutton(self.adv3, text='Green', padx=20, variable=self.col2, value='2').grid(column=0, row=3, sticky=W)
        Radiobutton(self.adv3, text='White', padx=20, variable=self.col2, value='3').grid(column=0, row=4, sticky=W)
        Radiobutton(self.adv3, text='Black', padx=20, variable=self.col2, value='4').grid(column=0, row=5, sticky=W)
        Radiobutton(self.adv3, text='Blue', padx=20, variable=self.col2, value='5').grid(column=0, row=6, sticky=W)
        Radiobutton(self.adv3, text='Orange', padx=20, variable=self.col2, value='6').grid(column=0, row=7, sticky=W)

        Button(self.adv3, text='Apply', command=self.applyADV3).grid(row=8, column=0, columnspan=2)

    def applyADV3(self):
        self.vis.chFColor = self.col2.get()

    def applyADV2(self):
        self.vis.chColor = self.col.get()
        try:
            self.vis.FluxSizeFac = float(self.fsf.get())
        except:
            messagebox.showerror('ERROR', '\'Size factor of flux value\' must be a number or float')
            return 0
        try:
            self.vis.roundn = int(float(self.roundn.get()))
        except:
            messagebox.showerror('ERROR', 'Decimals of stoichiometric factor must be a number.')
            return 0

        self.fsf.set(str(self.vis.FluxSizeFac))

    def advpar(self):
        self.adv = Toplevel()
        self.adv.title('Pathway Text Settings')
        w = 230
        h = 285
        x = (self.adv.winfo_screenwidth() - w) / 2
        y = (self.adv.winfo_screenheight() - h) / 2
        self.adv.geometry('%dx%d+%d+%d' % (w, h, x, y))
        Label(self.adv, text='Parameters for writing\nthe pathway on the image:').grid(row=0, column=0, columnspan=2)
        Label(self.adv, text=' Text ID for the\nwritten flux:').grid(row=1, column=0)
        self.idtv=StringVar()
        self.idt = Entry(self.adv, bg='white', textvariable=self.idtv, width=9)
        self.idt.grid(row=1, column=1)
        self.idtv.set(self.vis._newtextID)
        Label(self.adv, text='Write flux\nbelow/above header:').grid(row=2, column=0)
        self.bav = StringVar()
        self.ba = Entry(self.adv, bg='white', textvariable=self.bav, width=9)
        self.ba.grid(row=2, column=1)
        self.bav.set(str(self.vis._PathTXTlineheight))
        Label(self.adv, text='Size factor between\npathway-title and reactions:').grid(row=3, column=0)
        self.diffv = StringVar()
        self.diff = Entry(self.adv, bg='white', textvariable=self.diffv, width=9)
        self.diff.grid(row=3, column=1)
        self.diffv.set(str(self.vis._TitleReacDiff))
        Label(self.adv, text='Reactions per line:').grid(row=4, column=0)
        self.rplv = StringVar()
        self.rpl = Entry(self.adv, bg='white', textvariable=self.rplv, width=9)
        self.rpl.grid(row=4, column=1)
        self.rplv.set(str(self.vis._ReacPerLine))
        Label(self.adv, text='Title of the\nwritten pathway:').grid(row=5, column=0)
        self.pnv = StringVar()
        self.pn = Entry(self.adv, bg='white', textvariable=self.pnv, width=9)
        self.pn.grid(row=5, column=1)
        self.pnv.set(str(self.vis._PathName))

        Button(self.adv, text='Apply', command=self.applyADV).grid(row=6, column=0, columnspan=2)

    def multipar(self):
        self.adv = Toplevel()
        self.adv.title('Settings for multiple visualisations')
        w = 220
        h = 100
        x = (self.adv.winfo_screenwidth() - w) / 2
        y = (self.adv.winfo_screenheight() - h) / 2
        self.adv.geometry('%dx%d+%d+%d' % (w, h, x, y))
        Label(self.adv, text='File extension for\noutput svg file:').grid(row=0, column=0)
        self.multin=StringVar()
        self.multi = Entry(self.adv, bg='white', textvariable=self.multin, width=10)
        self.multi.grid(row=0, column=1)
        self.multin.set(self.vis.multifileext)

        Button(self.adv, text='Apply', command=self.applyMULTI).grid(row=1, column=0, columnspan=2)

    def applyMULTI(self):
        self.vis.multifileext = self.multin.get()
        self.multin.set(self.vis.multifileext)

    def applyADV(self):
        self.vis._newtextID = self.idtv.get()
        try:
            self.vis._PathTXTlineheight = int(float(self.ba.get()))
        except:
            messagebox.showerror('ERROR', 'Parameter \'Write EFM\nbelow/above header:\' must be an integer\n'
                                          '(Recommendation: 1 or -1)')
            return 0
        try:
            self.vis._TitleReacDiff = float(self.diff.get())
        except:
            messagebox.showerror('ERROR', 'Parameter: \'Size factor between\npathway-title and reactions\'\n'
                                          'must be a number or float.')
            return 0
        try:
            self.vis._ReacPerLine = int(float(self.rpl.get()))
        except:
            messagebox.showerror('ERROR', 'Parameter: \'Reactions per line\'\n'
                                          'must be an integer')
            return 0
        self.vis._PathName = self.pn.get()
        self.idtv.set(self.vis._newtextID)
        self.bav.set(str(self.vis._PathTXTlineheight))
        self.diffv.set(str(self.vis._TitleReacDiff))
        self.rplv.set(str(self.vis._ReacPerLine))
        self.pnv.set(str(self.vis._PathName))

    def visualize(self):
        try:
            fac = float(self.e2.get())
            self.vis.widthFactor = abs(fac)
        except ValueError:
            if not self.vis.dynW:
                messagebox.showerror('ERROR', '\'Width Factor\'EFM must be a Number.')
        if self.vis.dynW == 1:
            try:
                self.widthflux[0] = float(self.f1.get())
                self.widthflux[1] = float(self.f2.get())
                self.widthflux[2] = float(self.f3.get())
                self.widthflux[3] = float(self.w1.get())
                self.widthflux[4] = float(self.w2.get())
                self.widthflux[5] = float(self.w3.get())
                self.widthflux[6] = float(self.w4.get())
                self.f1.config(bg='white')
                self.f2.config(bg='white')
                self.f3.config(bg='white')
                self.w1.config(bg='white')
                self.w2.config(bg='white')
                self.w3.config(bg='white')
                self.w4.config(bg='white')
            except:
                messagebox.showerror('ERROR', 'All dynamic widths and fluxes must be numbers or floats.')
                self.f1.config(bg='red')
                self.f2.config(bg='red')
                self.f3.config(bg='red')
                self.w1.config(bg='red')
                self.w2.config(bg='red')
                self.w3.config(bg='red')
                self.w4.config(bg='red')
                return 0
            if not self.widthflux[0] <= self.widthflux[1] <= self.widthflux[2]:
                messagebox.showerror('ERROR', 'The dynamic flux values must be in an increasing order.')
                return 0

        if self.filename == '':
            messagebox.showerror('ERROR', 'No input svg file selected.')
            return 0

        if self.vis.dynW == 2:
            try:
                float(self.wmin.get())
                float(self.wmax.get())
                self.wmax.config(bg='white')
                self.wmin.config(bg='white')

            except ValueError:
                messagebox.showerror('ERROR', 'Min. and Max. values for the automatic width adaption must be numbers '
                                              'or floats.')
                self.wmax.config(bg='red')
                self.wmin.config(bg='red')
                return 0
            if float(self.wmin.get()) > float(self.wmax.get()):
                messagebox.showerror('ERROR', 'Min. value must be smaller than the Max. value.')
                self.wmax.config(bg='red')
                self.wmin.config(bg='red')
                return 0

        rule = self.e4.get()

        if 'REACTION' not in rule:
            messagebox.showerror('ERROR', 'ID format must contain the word \'REACTION\'.')
            self.e4.config(bg='red')
            return 0
        else:
            self.e4.config(bg='white')
        if 'COUNTER' not in rule:
            messagebox.showwarning('WARNING', 'It is highly recommended to set a counter for the IDs in the svg file,\n'
                                              'because multiple elements are not allowed to have the same ID.')
        self.vis.reacFormat = rule

        # New windows for further input:

        # Single pathway in Metatool format
        if self.inp.get() == 0:
            self.master2 = Toplevel()
            self.master2.protocol('WM_DELETE_WINDOW', self.closeM2)
            self.master.withdraw()
            self.master2.title('Define Pathway')
            Label(self.master2, text='Visualize a single pathway', font=(30)).grid(row=0, column=0)
            Label(self.master2, text='Type the pathway in the \nmetatool format down below:').grid(row=1, column=0)
            w = 300
            h = 350
            x = (self.master2.winfo_screenwidth() - w) / 2
            y = (self.master2.winfo_screenheight() - h) / 2
            self.master2.geometry('%dx%d+%d+%d' % (w, h, x, y))
            self.e1 = Text(self.master2, height=4, width=40, bg='white')
            self.e1.insert("1.0", 'e.g. Reac1 -Reac2 (6 Reac3) (-2.5 Reac4)')
            self.e1.grid(column=0, row=2, rowspan=4)
            savebutton = Button(self.master2, text='Save to', command=self.savefunc)
            savebutton.grid(column=0, row=6)
            Label(self.master2, textvariable=self.saved).grid(row=7, column=0)
            Label(self.master2, textvariable=self.showout).grid(row=8, column=0)
            Checkbutton(self.master2, variable=self.write, text='Write the Pathway on the image\n(Requires a '
                                                                '\'place_here\' element)').grid(column=0, row=9, sticky=W)
            Checkbutton(self.master2, variable=self.writef, text='Write flux values').grid(column=0, row=10, sticky=W)
            Button(self.master2, text='Visualize', command=self.vis_single, height=3, width=10,
                   font=self.butfont).grid(row=11, column=0, rowspan=2)

        # Complete metatool file
        if self.inp.get() == 1:
            self.master2 = Toplevel()
            self.master2.protocol('WM_DELETE_WINDOW', self.closeM2)
            self.master.withdraw()
            self.master2.title('Define metatool file')
            Label(self.master2, text='Visualize multiple pathways', font=(30)).grid(row=0, column=0)
            Label(self.master2, text='Open a metatool output file:').grid(row=1, column=0)
            w = 230
            h = 290
            x = (self.master2.winfo_screenwidth() - w) / 2
            y = (self.master2.winfo_screenheight() - h) / 2
            self.master2.geometry('%dx%d+%d+%d' % (w, h, x, y))

            self.met.set('Define a metatool file...')

            Label(self.master2, textvariable=self.met).grid(row=3, column=0)
            Button(self.master2, text='Metat. File', command=self.browsemeta).grid(column=0, row=2)
            savebutton = Button(self.master2, text='Save to', command=self.savefunc)
            savebutton.grid(column=0, row=6)
            self.saved.set('Define a folder to save output')
            Label(self.master2, textvariable=self.saved).grid(row=7, column=0)
            Label(self.master2, textvariable=self.showout).grid(row=8, column=0)
            Checkbutton(self.master2, variable=self.write, text='Write the pathway on the image\n(Requires a '
                                                                '\'place_here\' element)').grid(column=0, row=9, sticky=W)
            Checkbutton(self.master2, variable=self.writef, text='Write flux values').grid(column=0, row=10, sticky=W)
            Button(self.master2, text='Visualize', command=self.vis_meta, height=3, width=10,
                   font=self.butfont).grid(row=11, column=0, rowspan=2)

        # Single path in CNA list export format
        if self.inp.get() == 2:
            self.master2 = Toplevel()
            self.master2.protocol('WM_DELETE_WINDOW', self.closeM2)
            self.master.withdraw()
            self.master2.title('Define Pathway')
            Label(self.master2, text='Visualize a single pathway', font=(30)).grid(row=0, column=0)
            Label(self.master2, text='Type the pathway in the \nCNA-list format down below:').grid(row=1, column=0)
            w = 300
            h = 290
            x = (self.master2.winfo_screenwidth() - w) / 2
            y = (self.master2.winfo_screenheight() - h) / 2
            self.master2.geometry('%dx%d+%d+%d' % (w, h, x, y))
            self.e1 = Text(self.master2, height=4, width=40, bg='white')
            self.e1.insert("1.0", 'e.g.  -1 Reac1  1 Reac2  6 Reac3 ')
            self.e1.grid(column=0, row=2, rowspan=4)
            savebutton = Button(self.master2, text='Save to', command=self.savefunc)
            savebutton.grid(column=0, row=6)
            Label(self.master2, textvariable=self.saved).grid(row=7, column=0)
            Label(self.master2, textvariable=self.showout).grid(row=8, column=0)
            Checkbutton(self.master2, variable=self.write, text='Write the Pathway on the image\n(Requires a '
                                                                '\'place_here\' element)').grid(column=0, row=9, sticky=W)
            Checkbutton(self.master2, variable=self.writef, text='Write flux values').grid(column=0, row=10, sticky=W)
            Button(self.master2, text='Visualize', command=self.vis_cnal, height=3, width=10,
                   font=self.butfont).grid(row=11, column=0, rowspan=2)

        # Complete CNA list export file
        if self.inp.get() == 3:
            self.master2 = Toplevel()
            self.master2.protocol('WM_DELETE_WINDOW', self.closeM2)
            self.master.withdraw()
            self.master2.title('Define CNA EFM-list file')
            Label(self.master2, text='Visualize multiple pathways', font=(30)).grid(row=0, column=0)
            Label(self.master2, text='Open a CNA EFM-list file:').grid(row=1, column=0)
            w = 230
            h = 290
            x = (self.master2.winfo_screenwidth() - w) / 2
            y = (self.master2.winfo_screenheight() - h) / 2
            self.master2.geometry('%dx%d+%d+%d' % (w, h, x, y))
            self.met = StringVar()
            self.met.set('Define a CNA EFM-list file...')

            Label(self.master2, textvariable=self.met).grid(row=3, column=0)
            Button(self.master2, text='CNA File', command=self.browsemeta).grid(column=0, row=2)
            savebutton = Button(self.master2, text='Save to', command=self.savefunc)
            savebutton.grid(column=0, row=6)
            self.saved.set('Define a folder to save output')
            Label(self.master2, textvariable=self.saved).grid(row=7, column=0)
            Label(self.master2, textvariable=self.showout).grid(row=8, column=0)
            Checkbutton(self.master2, variable=self.write, text='Write the pathway on the image\n(Requires a '
                                                                '\'place_here\' element)').grid(column=0, row=9, sticky=W)
            Checkbutton(self.master2, variable=self.writef, text='Write flux values').grid(column=0, row=10, sticky=W)
            Button(self.master2, text='Visualize', command=self.vis_cnal2, height=3, width=10,
                   font=self.butfont).grid(row=11, column=0, rowspan=2)

        # COPASI export file:
        if self.inp.get() == 4:
            self.master2 = Toplevel()
            self.master2.protocol('WM_DELETE_WINDOW', self.closeM2)
            self.master.withdraw()
            self.master2.title('Define COPASI export file')
            Label(self.master2, text='Visualize a single flux', font=(30)).grid(row=0, column=0)
            Label(self.master2, text='Open a COPASI export file:').grid(row=1, column=0)
            w = 230
            h = 290
            x = (self.master2.winfo_screenwidth() - w) / 2
            y = (self.master2.winfo_screenheight() - h) / 2
            self.master2.geometry('%dx%d+%d+%d' % (w, h, x, y))

            self.met.set('Define a COPASI export file...')

            Label(self.master2, textvariable=self.met).grid(row=3, column=0)
            Button(self.master2, text='COPASI File', command=self.browsemeta).grid(column=0, row=2)
            savebutton = Button(self.master2, text='Save to', command=self.savefunc)
            savebutton.grid(column=0, row=6)
            Label(self.master2, textvariable=self.saved).grid(row=7, column=0)
            Label(self.master2, textvariable=self.showout).grid(row=8, column=0)
            Checkbutton(self.master2, variable=self.write, text='Write the pathway on the image\n(Requires a '
                                                                '\'place_here\' element)').grid(column=0, row=9, sticky=W)
            Checkbutton(self.master2, variable=self.writef, text='Write flux values').grid(column=0, row=10, sticky=W)
            Button(self.master2, text='Visualize', command=self.vis_cop, height=3, width=10,
                   font=self.butfont).grid(row=11, column=0, rowspan=2)

        # F-A-M-E export file:
        if self.inp.get() == 5:
            self.master2 = Toplevel()
            self.master2.protocol('WM_DELETE_WINDOW', self.closeM2)
            self.master.withdraw()
            self.master2.title('Define FAME export file')
            Label(self.master2, text='Visualize a single flux', font=(30)).grid(row=0, column=0)
            Label(self.master2, text='Open a FAME export file:').grid(row=1, column=0)
            w = 230
            h = 290
            x = (self.master2.winfo_screenwidth() - w) / 2
            y = (self.master2.winfo_screenheight() - h) / 2
            self.master2.geometry('%dx%d+%d+%d' % (w, h, x, y))

            self.met.set('Define a FAME export file...')

            Label(self.master2, textvariable=self.met).grid(row=3, column=0)
            Button(self.master2, text='FAME File', command=self.browsemeta).grid(column=0, row=2)
            savebutton = Button(self.master2, text='Save to', command=self.savefunc)
            savebutton.grid(column=0, row=6)
            Label(self.master2, textvariable=self.saved).grid(row=7, column=0)
            Label(self.master2, textvariable=self.showout).grid(row=8, column=0)
            Checkbutton(self.master2, variable=self.write, text='Write the pathway on the image\n(Requires a '
                                                                '\'place_here\' element)').grid(column=0, row=9,
                                                                                                sticky=W)
            Checkbutton(self.master2, variable=self.writef, text='Write flux values').grid(column=0, row=10,
                                                                                           sticky=W)
            Button(self.master2, text='Visualize', command=self.vis_cop, height=3, width=10,
                   font=self.butfont).grid(row=11, column=0, rowspan=2)

    def vis_single(self):
        if self.outfile == '':
            messagebox.showerror('ERROR', 'No output file selected.')
            return 0
        try:
            self.vis.input_path(self.e1.get("1.0", END).strip())
        except:
            messagebox.showerror('ERROR', 'No valid pathway format')
            return 0
        if self.vis.dynW == 1:
            self.vis.make_widthl(self.widthflux[0], self.widthflux[1], self.widthflux[2], self.widthflux[3],
                                 self.widthflux[4], self.widthflux[5], self.widthflux[6])
        elif self.vis.dynW == 2:
            self.vis.make_autowidth(float(self.wmin.get()), float(self.wmax.get()), logarithmic=bool(self.logarithmic.get()))

        self.vis.visualize_path()
        self.vis.round_stoich()
        if self.write.get():
            self.vis.write_path(self.vis._PathName)
        if self.writef.get():
            self.vis.write_flux()
        self.vis.save(self.outfile)
        self.vis.path = []
        self.vis.input_file(self.filename)
        self.vis.getall_children()
        messagebox.showinfo('Success!', 'Successfully visualized the pathway:\n' +
                            str(self.e1.get("1.0", END).strip())+'\nThe result was saved into the file:\n'+self.outfile)
        self.master2.destroy()
        self.master.deiconify()

    def vis_cnal(self):
        if self.outfile == '':
            messagebox.showerror('ERROR', 'No output file selected.')
            return 0
        try:
            self.vis.input_CNApath(self.e1.get("1.0", END).strip())
        except:
            messagebox.showerror('ERROR', 'No valid pathway format')
            return 0
        if self.vis.dynW == 1:
            self.vis.make_widthl(self.widthflux[0], self.widthflux[1], self.widthflux[2], self.widthflux[3],
                                 self.widthflux[4], self.widthflux[5], self.widthflux[6])
        elif self.vis.dynW == 2:
            self.vis.make_autowidth(float(self.wmin.get()), float(self.wmax.get()), logarithmic=bool(self.logarithmic.get()))

        self.vis.visualize_path()
        self.vis.round_stoich()
        if self.write.get():
            self.vis.write_path(self.vis._PathName)
        if self.writef.get():
            self.vis.write_flux()
        self.vis.save(self.outfile)
        self.vis.path = []
        self.vis.input_file(self.filename)
        self.vis.getall_children()
        messagebox.showinfo('Success!', 'Successfully visualized the pathway:\n' +
                            str(self.e1.get("1.0", END).strip())+'\nThe result was saved into the file:\n'+self.outfile)
        self.master2.destroy()
        self.master.deiconify()

    def vis_meta(self):
        if self.outfile == '':
            messagebox.showerror('ERROR', 'No output file selected.')
            return 0
        try:
            self.vis.parse_metatool(self.metaf)
            self.vis.visualize_all(self.outfile, write=self.write.get(), writef=self.writef.get(),
                                   widthpar=self.widthflux, autow=[float(self.wmin.get()), float(self.wmax.get())], logarithmic=bool(self.logarithmic.get()))
        except:
            messagebox.showerror('ERROR', 'File has the wrong format.\n Try to choose a diferent visualization input.')
            return 0
        messagebox.showinfo('Success!', 'Successfully visualized '+str(self.vis.EFMl)+' pathways.')
        self.master2.destroy()
        self.master.deiconify()

    def vis_cop(self):
        if self.outfile == '':
            messagebox.showerror('ERROR', 'No output file selected.')
            return 0

        try:
            self.vis.input_copasi(self.metaf)
        except:
            messagebox.showerror('ERROR', 'No valid COPASI export file')
            return 0
        if self.vis.dynW == 1:
            self.vis.make_widthl(self.widthflux[0], self.widthflux[1], self.widthflux[2], self.widthflux[3],
                                 self.widthflux[4], self.widthflux[5], self.widthflux[6])
        elif self.vis.dynW == 2:
            self.vis.make_autowidth(float(self.wmin.get()), float(self.wmax.get()), logarithmic=bool(self.logarithmic.get()))

        self.vis.visualize_path()
        self.vis.round_stoich()
        if self.write.get():
            self.vis.write_path(self.vis._PathName)
        if self.writef.get():
            self.vis.write_flux()
        self.vis.save(self.outfile)
        self.vis.path = []
        self.vis.input_file(self.filename)
        self.vis.getall_children()
        messagebox.showinfo('Success!', 'Successfully visualized the flux!+\nThe result was saved into the file:\n' +
                            self.outfile)
        self.master2.destroy()
        self.master.deiconify()

    def vis_cnal2(self):
        if self.outfile == '':
            messagebox.showerror('ERROR', 'No output file selected.')
            return 0
        try:
            self.vis.parse_CNAlist(self.metaf)
            self.vis.visualize_cnal(self.outfile, write=self.write.get(), writef=self.writef.get(), widthpar=self.widthflux,
                                    autow=[float(self.wmin.get()), float(self.wmax.get())], logarithmic=bool(self.logarithmic.get()))
        except:
            messagebox.showerror('ERROR', 'File has the wrong format.\n Try to choose a diferent visualization input.')
            return 0
        messagebox.showinfo('Success!', 'Successfully visualized '+str(self.vis.EFMl)+' pathways.')
        self.master2.destroy()
        self.master.deiconify()

    def show(self):
        self.master.mainloop()

    def savefunc(self):
        self.outfile = filedialog.asksaveasfilename(defaultextension='.svg')
        try:
            self.saved.set('Result will be saved in:')
            self.showout.set('.../' + self.outfile.split('/')[-1])
        except:
            messagebox.showerror('ERROR', 'No file defined.')

    def browsefunc(self):
        self.filename = filedialog.askopenfilename()
        try:
            self.vis.input_file(self.filename)
            self.vis.getall_children()
            self.showfile.set('.../' + self.filename.split('/')[-1])
            self.opened.set('successfully opened file:')
        except Exception:
            messagebox.showerror('ERROR', 'EFMvisualizer was not able to read the given file.\nMake sure it is a '
                                          'SVG-file')

    def browsemeta(self):
        self.metaf = filedialog.askopenfilename()
        try:
            self.met.set('.../' + self.metaf.split('/')[-1])
        except Exception:
            messagebox.showerror('ERROR', 'EFMvisualizer was not able to read the given file.')

    def closeM2(self):
        self.master2.destroy()
        self.master.deiconify()
        self.met.set('Define a metatool file...')
        self.metaf = ''
        self.saved.set('Define a file to save output')
        self.showout.set('')


def main():
    gui = GUI()
    gui.show()

if __name__ == '__main__':
    main()
